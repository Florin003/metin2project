package com.metin.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metin.main.Clasament;

@Repository
public class ClasamentDao {

	@Autowired
	 private SessionFactory sessionFactory;
	
	public List<Clasament> getAllClasament() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Clasament");
		
		List<Clasament> clasament = query.getResultList();
		
		return clasament;
		
	}
}
