package com.metin.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metin.main.Contact;



@Repository
public class ContactDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List <Contact> getAllContact() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Contact");
		
		List<Contact> contact = query.getResultList();
		
		return contact;
	}
}
