package com.metin.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metin.main.Bonusuri;

@Repository
public class BonusuriDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Bonusuri> getAllBonusuri() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Bonusuri");
		
		List<Bonusuri> bonusuri = query.getResultList();
		
		return bonusuri;
	}
}
