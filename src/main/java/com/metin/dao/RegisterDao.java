package com.metin.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metin.main.Register;

@Repository
public class RegisterDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<Register> getAllRegister() {

		Query query = sessionFactory.getCurrentSession().createQuery("from Register");
		List<Register> register = query.getResultList();

		return register;
	}

	public void saveRegister(Register register) {
		sessionFactory.getCurrentSession().save(register);

	}

	public Register getRegisterById(int registerId) {

		Register register = sessionFactory.getCurrentSession().get(Register.class, registerId);

		return register;
	}

	
	public void updateRegister(Register register) {
		sessionFactory.getCurrentSession().saveOrUpdate(register);

	}

}
