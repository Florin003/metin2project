package com.metin.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metin.main.User;

@Repository
public class LoginDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public List<User> getAllLogin() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Login");
		List<User> user = query.getResultList();
		
		return user;
	}

	
	public Integer saveLogin(User user) {
		Integer id = (Integer) sessionFactory.getCurrentSession().save(user);
		System.out.println("id in dao " + id);
		return id;
	}
	
	
	public User getLoginById(int loginId) {
		
		User user = sessionFactory.getCurrentSession().get(User.class, loginId);
		
		return user;
	}

}
