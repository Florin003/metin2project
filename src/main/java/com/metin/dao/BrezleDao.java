package com.metin.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metin.main.Brezle;

@Repository
public class BrezleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Brezle> getAllBrezle() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Brezle");
		
		List<Brezle> brezle = query.getResultList();
		
		return brezle;
		
	}
}
