package com.metin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.metin.dao.BonusuriDao;
import com.metin.main.Bonusuri;

@Service
@Transactional
public class BonusuriService {

	@Autowired
	private BonusuriDao bonusuriDao;
	
	public List<Bonusuri> getAllBonusuri() {
	
	 List<Bonusuri> bonusuri = bonusuriDao.getAllBonusuri();
	 
	 return bonusuri;
	 
	}
}
