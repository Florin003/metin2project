package com.metin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.metin.dao.ContactDao;
import com.metin.main.Contact;

@Service
@Transactional
public class ContactService {

	@Autowired
	private ContactDao contactDao;
	
	public List <Contact> getAllContact() {
		
		List <Contact> contact = contactDao.getAllContact();
		
		return contact;
	}
}
