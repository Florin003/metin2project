package com.metin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.metin.dao.RegisterDao;
import com.metin.main.Register;
import com.metin.main.User;

@Service
@Transactional
public class RegisterService {

	@Autowired
	private RegisterDao registerDao;
	
	@Autowired
	private LoginService loginService;
	
	
	public List<Register> getAllRegister() {
		List<Register> register = registerDao.getAllRegister();
		
		return register;
	}

	
	public void saveRegister(String username, String password, String gender, String email,
			String phone) {
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setEmail(email);
		
		
		Register register = new Register();
		register.setUsername(username);
		register.setPassword(password);
		register.setEmail(email);
		register.setPhone(phone);
		register.setGender(gender);
		
		registerDao.saveRegister(register);
		Integer userId = loginService.saveUser(user);
		
		System.out.println(userId);
		
//		UserRole userRole = new UserRole();
//		userRole.setRoleId(Role.CLIENT.getRoleId());
//		user = loginService.getLoginById(userId);
//		userRole.setUserRoleId(userId);		
//		
//		userRoleService.saveUserRole(userRole);

	}

	
	public Register getRegisterById(int registerId) {
		Register register =registerDao.getRegisterById(registerId);
		
		return register;
	}

	
	public void updateRegister(int registerId, String username, String password, String gender, String email,
			String phone) {
		
		Register register =registerDao.getRegisterById(registerId);
		register.setRegisterId(registerId);
		register.setUsername(username);
		register.setPassword(password);
		register.setGender(gender);
		register.setEmail(email);
		register.setPhone(phone);
		
		registerDao.updateRegister(register);
	}

	public RegisterDao getRegisterDao() {
		return registerDao;
	}

	public void setRegisterDao(RegisterDao registerDao) {
		this.registerDao = registerDao;
	}
}
