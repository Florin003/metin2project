package com.metin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.metin.dao.ClasamentDao;
import com.metin.main.Clasament;

@Service
@Transactional
public class ClasamentService {

	@Autowired
	private ClasamentDao clasamentDao;
	
	public List<Clasament> getAllClasament() {
		
		List<Clasament> clasament = clasamentDao.getAllClasament();
		
		return clasament;
	}
}
