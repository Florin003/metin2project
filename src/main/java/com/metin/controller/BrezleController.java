package com.metin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metin.main.Brezle;
import com.metin.service.BrezleService;

@Controller
@RequestMapping("/brezle")
public class BrezleController {

	@Autowired
	private BrezleService brezleService;
	
	@RequestMapping("/list")
	public String listBrezle(ModelMap model) {
		List <Brezle> brezle = brezleService.getAllBrezle();
		
		model.addAttribute("brezleList", brezle);
		
		return "brezleListView";
	}
}
