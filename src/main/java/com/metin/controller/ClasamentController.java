package com.metin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metin.main.Clasament;
import com.metin.service.ClasamentService;

@Controller
@RequestMapping("/clasament")
public class ClasamentController {

	@Autowired
	private ClasamentService clasamentService;
	
	@RequestMapping("/list")
	public String listClasament(ModelMap model) {
		List <Clasament> clasament = clasamentService.getAllClasament();
		
		model.addAttribute("clasamentList", clasament);
		
		return "clasamentListView";
	}
}
