package com.metin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metin.main.Bonusuri;
import com.metin.service.BonusuriService;



@Controller
@RequestMapping("/bonusuri")
public class BonusuriController {
	
	@Autowired
	private BonusuriService bonusuriService;
	
	@RequestMapping("/list")
	public String listBonusuri(ModelMap model) {
		List<Bonusuri> bonusuri = bonusuriService.getAllBonusuri();
		
		model.addAttribute("bonusuriList", bonusuri);
		
		return "bonusuriListView";
	}

}
