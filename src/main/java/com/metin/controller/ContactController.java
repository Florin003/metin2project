package com.metin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metin.main.Contact;
import com.metin.service.ContactService;



@Controller
@RequestMapping("/contact")
public class ContactController {

	@Autowired
	 private ContactService contactService;
	
	@RequestMapping("/list")
	public String listContact(ModelMap model) {
		List<Contact> contact = contactService.getAllContact();
		
		model.addAttribute("contactList", contact);
		
		return "contactListView";
	}
}
