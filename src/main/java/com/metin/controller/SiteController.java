package com.metin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/site")
public class SiteController {

	@RequestMapping("")
	public String listSite(ModelMap model) {

		return "site";
	}

}
