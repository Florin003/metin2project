package com.metin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/download")
public class DownloadController {

	@RequestMapping("")
	public String listSite(ModelMap model) {

		return "download";
	}
}
