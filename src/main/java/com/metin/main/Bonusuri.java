package com.metin.main;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.RequestMapping;

@Entity
@Table(name="bonusuri")
public class Bonusuri {

	@Id
	private int BonusId;
	private String Lovitura;
	private String SemiOm;
	private String MaxHp;
	private String Bonusuri;
	private String Aparari;
	private String PgmMax;
	
	public Bonusuri() {
		super();
	}

	public Bonusuri(int bonusId, String lovitura, String semiOm, String maxHp, String bonusuri, String aparari,
			String pgmMax) {
		super();
		BonusId = bonusId;
		Lovitura = lovitura;
		SemiOm = semiOm;
		MaxHp = maxHp;
		Bonusuri = bonusuri;
		Aparari = aparari;
		PgmMax = pgmMax;
	}

	public int getBonusId() {
		return BonusId;
	}

	public void setBonusId(int bonusId) {
		BonusId = bonusId;
	}

	public String getLovitura() {
		return Lovitura;
	}

	public void setLovitura(String lovitura) {
		Lovitura = lovitura;
	}

	public String getSemiOm() {
		return SemiOm;
	}

	public void setSemiOm(String semiOm) {
		SemiOm = semiOm;
	}

	public String getMaxHp() {
		return MaxHp;
	}

	public void setMaxHp(String maxHp) {
		MaxHp = maxHp;
	}

	public String getBonusuri() {
		return Bonusuri;
	}

	public void setBonusuri(String bonusuri) {
		Bonusuri = bonusuri;
	}

	public String getAparari() {
		return Aparari;
	}

	public void setAparari(String aparari) {
		Aparari = aparari;
	}

	public String getPgmMax() {
		return PgmMax;
	}

	public void setPgmMax(String pgmMax) {
		PgmMax = pgmMax;
	}

	@Override
	public String toString() {
		return "Bonusuri [BonusId=" + BonusId + ", Lovitura=" + Lovitura + ", SemiOm=" + SemiOm + ", MaxHp=" + MaxHp
				+ ", Bonusuri=" + Bonusuri + ", Aparari=" + Aparari + ", PgmMax=" + PgmMax + "]";
	}
	
}
