package com.metin.main;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contact")
public class Contact {
	
	@Id
	private int contactId; 
	private String NumeAdmin; 
	private String Grad;
	private int numarTelefon;
	private String email;
	
	
	public Contact() {
		super();
	}

	public Contact(int contactId, String numeAdmin, String grad, int numarTelefon, String email) {
		super();
		this.contactId = contactId;
		NumeAdmin = numeAdmin;
		Grad = grad;
		this.numarTelefon = numarTelefon;
		this.email = email;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getNumeAdmin() {
		return NumeAdmin;
	}

	public void setNumeAdmin(String numeAdmin) {
		NumeAdmin = numeAdmin;
	}

	public String getGrad() {
		return Grad;
	}

	public void setGrad(String grad) {
		Grad = grad;
	}

	public int getNumarTelefon() {
		return numarTelefon;
	}

	public void setNumarTelefon(int numarTelefon) {
		this.numarTelefon = numarTelefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", NumeAdmin=" + NumeAdmin + ", Grad=" + Grad + ", numarTelefon="
				+ numarTelefon + ", email=" + email + "]";
	}

	
}
