package com.metin.main;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name= "bresle")
public class Brezle {

	@Id
	private int bresleId;
	private String NumeBreasla;	 
	private int LevelBreasla; 
	private int RazboiCastigat; 
	private int RazboiPierdut;
	
	
	public Brezle() {
		super();
	}

	public Brezle(int bresleId, String numeBreasla, int levelBreasla, int razboiCastigat, int razboiPierdut) {
		super();
		this.bresleId = bresleId;
		NumeBreasla = numeBreasla;
		LevelBreasla = levelBreasla;
		RazboiCastigat = razboiCastigat;
		RazboiPierdut = razboiPierdut;
	}

	public int getBresleId() {
		return bresleId;
	}

	public void setBresleId(int bresleId) {
		this.bresleId = bresleId;
	}

	public String getNumeBreasla() {
		return NumeBreasla;
	}

	public void setNumeBreasla(String numeBreasla) {
		NumeBreasla = numeBreasla;
	}

	public int getLevelBreasla() {
		return LevelBreasla;
	}

	public void setLevelBreasla(int levelBreasla) {
		LevelBreasla = levelBreasla;
	}

	public int getRazboiCastigat() {
		return RazboiCastigat;
	}

	public void setRazboiCastigat(int razboiCastigat) {
		RazboiCastigat = razboiCastigat;
	}

	public int getRazboiPierdut() {
		return RazboiPierdut;
	}

	public void setRazboiPierdut(int razboiPierdut) {
		RazboiPierdut = razboiPierdut;
	}

	@Override
	public String toString() {
		return "Brezle [bresleId=" + bresleId + ", NumeBreasla=" + NumeBreasla + ", LevelBreasla=" + LevelBreasla
				+ ", RazboiCastigat=" + RazboiCastigat + ", RazboiPierdut=" + RazboiPierdut + "]";
	} 
	
}
