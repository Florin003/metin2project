package com.metin.main;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="clasament")
public class Clasament {

	@Id
	private int clasamentId;
	private String numeJucator;
	private int levelJucator;
	private int timpJoc;
	
	public Clasament() {
		super();
	}

	public Clasament(int clasamentId, String numeJucator, int levelJucator, int timpJoc) {
		super();
		this.clasamentId = clasamentId;
		this.numeJucator = numeJucator;
		this.levelJucator = levelJucator;
		this.timpJoc = timpJoc;
	}

	public int getClasamentId() {
		return clasamentId;
	}

	public void setClasamentId(int clasamentId) {
		this.clasamentId = clasamentId;
	}

	public String getNumeJucator() {
		return numeJucator;
	}

	public void setNumeJucator(String numeJucator) {
		this.numeJucator = numeJucator;
	}

	public int getLevelJucator() {
		return levelJucator;
	}

	public void setLevelJucator(int levelJucator) {
		this.levelJucator = levelJucator;
	}

	public int getTimpJoc() {
		return timpJoc;
	}

	public void setTimpJoc(int timpJoc) {
		this.timpJoc = timpJoc;
	}

	@Override
	public String toString() {
		return "Clasament [clasamentId=" + clasamentId + ", numeJucator=" + numeJucator + ", levelJucator="
				+ levelJucator + ", timpJoc=" + timpJoc + "]";
	}
	
	
}
